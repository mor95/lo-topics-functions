const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Some arguments are expected.')

    if(!_.isObject(args.loProgress))
        throw new Error('An loProgress instance is expected.')

    if(!_.isObject(args.topicsFunctions))
        console.info('No topics functions received.')

    var self = this
    self.loProgress = args.loProgress
    self.topicsFunctions = args.topicsFunctions || {}
    self.startingTopics = args.loProgress.topics || {}
    self.topicsEffects = {}

    _.forEach(self.topicsFunctions, function(v, k){
        self.topicsFunctions[k] = v
    })

    self.do = function(topics){
        if(!_.isObject(topics))
            throw new Error('Some topic properties are expected.')

        _.forEach(topics.finished, function(v, k){
            if(self.topicsEffects[k] === true) return true
            self.topicsEffects[k] = true
    
            _.isFunction(self.topicsFunctions[k]) &&
                self.topicsFunctions[k](topics)
        })
    }

    self.do(self.startingTopics)

    self.loProgress.events.on('update', function(topics){
        self.do(topics)

        if(_.size(topics.pending()) === 0){
            if(_.isObject(self.topicsFunctions) && _.isFunction(self.topicsFunctions.allDone)){
                if(self.finishedOva) return true
                self.finishedOva = true
                self.topicsFunctions.allDone(topics)
            }
        }
    })
}